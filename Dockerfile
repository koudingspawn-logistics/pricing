FROM maven:3.6-jdk-11 AS build

COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package

FROM gcr.io/distroless/java:11

COPY --from=build /usr/src/app/target/pricing.jar /app.jar
CMD ["/app.jar"]